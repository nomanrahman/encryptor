#!/usr/bin/env bash

echo "Note: this should be run from the project root directory"

version="latest"

if [ $# -gt 0 ]; then
  version=$1
fi

docker \
  build -t hspconsortium/encryptor:${version} \
  --build-arg JAR_FILE=target/*.jar \
  .
