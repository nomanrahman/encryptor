package org.hspconsortium.controller;

import org.hspconsortium.model.EncryptionRequestDto;
import org.hspconsortium.service.EncryptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class EncryptionController {

    @Autowired()
    private EncryptionService encryptionService;

    @RequestMapping(path = "encryptForJava", method = RequestMethod.POST)
    @ResponseBody
    private EncryptionRequestDto encryptForJava(@RequestBody EncryptionRequestDto encryptionRequestDto) {
        encryptionRequestDto.setOut(encryptionService.encryptForJava(encryptionRequestDto.getIn()));
        return encryptionRequestDto;
    }

    @RequestMapping(path = "encryptForNodejs", method = RequestMethod.POST)
    @ResponseBody
    private EncryptionRequestDto encryptForNodejs(@RequestBody EncryptionRequestDto encryptionRequestDto) {
        encryptionRequestDto.setOut(encryptionService.encryptForNodejs(encryptionRequestDto.getIn()));
        return encryptionRequestDto;
    }
}
